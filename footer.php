    <footer id="footer"><!-- 上位命令を発行したいときはidでcss記述 -->
      <article class="adress">
        <p><a href="#contact" class="modal">運営会社：大和リビング株式会社</a></p>
      </article>
      <article class="copyright">
<?php
  if (date('Y') > 2019) {
    $date = "2019-" + date('Y');
  } else {
    $date = date('Y');
  }
?>
        <p>Copyright &copy; <?php echo $date; ?> HAKOVIVA. All Rights Reserved.</p>
      </article>
    </footer><!-- /footer -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="./common/js/flexibility.js"></script>
    <script src="./common/js/jquery.cookie.js"></script>
    <script src="./common/js/common.js"></script>
    <script src="./common/js/custom.js"></script>

    <div class="modal_contents">
      <div id="contact">
        <div class="box contactarea">
          <h3>大和リビング株式会社賃貸事業推進部</h3>
          <p class="tac fas fa-phone-alt fs2"><a href="tel:0355006506">03-5500-6506</a></p>
          <p class="asterisk">問い合わせいただいた内容に合わせて担当者から連絡いたします。</p>
          <p><a href="https://www.daiwaliving.co.jp/privacy/privacy_policy/" target="_blank">プライバシーポリシー</a>はこちら</p>
        </div>
      </div>
    </div>
