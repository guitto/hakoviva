<!DOCTYPE html>
<html lang="ja">
  <head>
<?php
  // 初期設定
  $set_id = "shop";
  $title_name = "箱館ジンギスカン&nbsp;函館駅前店";
?>
<?php include './head.php'; ?>
    <link href="./css/shop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./common/css/swiper.css">
    <script src="./common/js/swiper.min.js"></script>

    <meta name="robots" content="noindex">
    <meta name="robots" content="nofollow">
  </head>

  <body id="<?php echo $set_id; ?>">
<?php include './header.php'; ?>
    <main id="main">
      <article>
        <section class="shop_detail">
          <div class="flex_pc">
            <div class="imagearea">
              <h1 class="title grow2"><i class="fas fa-utensils food"><?php /*
<i class="fas fa-utensils food"> //グルメ＆フード
<i class="fas fa-shopping-bag shop"> //グッズ＆ショップ
<i class="fas fa-hotel"> //ホテル
><i class="fas fa-dumbbell">　//ジム
<i class="fas fa-utensils other"> //その他
              */ ?></i><?php echo $title_name; ?></h1>
              <div id="branding">
                <div class="swiper-container gallery-top">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img1.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img2.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img3.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img4.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img5.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img6.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img7.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img8.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img9.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img10.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img11.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img12.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img13.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img14.jpg)"></div>
                    <div class="swiper-slide img_area" style="background-image:url(./images/test/img15.jpg)"></div>
                  </div>
                  <!-- Add Arrows -->
                  <div class="swiper-button-next swiper-button-white"></div>
                  <div class="swiper-button-prev swiper-button-white"></div>
                </div>
                <div class="swiper-container gallery-thumbs">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image:url(./images/test/img1.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img2.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img3.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img4.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img5.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img6.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img7.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img8.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img9.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img10.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img11.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img12.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img13.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img14.jpg)"></div>
                    <div class="swiper-slide" style="background-image:url(./images/test/img15.jpg)"></div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <ul class="flex column3 h_gap">
                <li class="food">グルメ&amp;フード</li>
<?php /*
                <li class="food">グルメ&amp;フード</li>
                <li class="shop">グッズ&amp;ショップ</li>
                <li class="other">その他</li>
*/ ?>
                <li class="yokocho">函館駅前横丁</li>
<?php /*
                <li class="hotel">ホテルエリア</li>
                <li class="yokocho">函館駅前横丁</li>
                <li class="cafe">ゲートサイド</li>
                <li class="gym">
                スポーツジムエリア</li>
*/ ?>
                <li class="new">New</li><?php // on, offのみ ?>
              </ul>
              <h2 class="image box"><img src="./common/img/yokocho_logo.svg" width="334" height="73" alt="ハコビバ HAKOVIVA"></h2>
              <p>２０１９年１２月にNEW OPENした「箱館ジンギスカン　函館駅前横丁店」仔羊肉専門店として注目のお店！！単品メニューも豊富で、お得な食べ放題・飲み放題（２名様から）プランもご用意している。家族やご友人とゆったり小上がり個室や掘りごたつテーブル席で食事をお楽しみください、団体席（３２名分）もご用意しているので宴会などの利用にも最適。</p>
              <h3>店舗情報</h3>
              <dl class="flex shop_data wrap">
                <dt>店舗名</dt>
                <dd>箱館ジンギスカン&nbsp;函館駅前店</dd>
                <dt>カテゴリ</dt>
                <dd>グルメ&amp;フード</dd>
                <dt>電話番号</dt>
                <dd>000-000-0000</dd>
                <dt>営業時間</dt>
                <dd>10:00～24:00</dd>
                <dt>定休日</dt>
                <dd>なし</dd>
                <dt>公式サイト</dt>
                <dd><a href="https://tabelog.com/hokkaido/A0105/A010501/1062853/">箱館ジンギスカン</a></dd>
              </dl>
            </div>
          </div>
          <script>
            var $width = window.innerWidth;
            var $spacing = 16;
            if ($width < 768) {
              $spacing = 0;
            }
            var galleryThumbs = new Swiper('.gallery-thumbs', {
              loop: true,
              spaceBetween: $spacing,
              slidesPerView: 3,
              freeMode: true,
              watchSlidesVisibility: true,
              watchSlidesProgress: true,
            });
            var galleryTop = new Swiper('.gallery-top', {
              speed: 300,
              autoplay: {
                delay: 5000,
                disableOnInteraction: false,
              },
              loop: true,
              spaceBetween: $spacing,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
              thumbs: {
                swiper: galleryThumbs
              }
            });
          </script>
        </section>
      </article>
      <article id="food" class="shop_list">
        <section>
          <h2 class="title"><i class="fas fa-utensils"></i>Gourmets &amp; Foods<span>グルメ＆フード</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
<?php include './main_common.php'; ?>
    </main>
<?php /*
コンテンツ領域1280px （SP時に640px、320pxに可変しやすく綺麗に見える）
アートボード1440px Macbook15.4インチに丁度いい
*/ ?>
<?php include './footer.php'; ?>
    <script src="./js/index.js"></script>
  </body>
</html>
