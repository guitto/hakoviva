<!DOCTYPE html>
<html lang="ja">
  <head>
<?php
  // 初期設定
  $set_id = "shop";
  $title_name = "集まる、広がる。この「場」から。";
?>
<?php include './head.php'; ?>
    <link href="./css/shop.css" rel="stylesheet" type="text/css">

    <meta name="robots" content="noindex">
    <meta name="robots" content="nofollow">
  </head>

  <body id="<?php echo $set_id; ?>">
<?php include './header.php'; ?>
    <main id="main">
      <article id="food" class="shop_list">
        <section>
          <h1 class="title">Shop<br><span>店舗紹介</span></h1>
          <h2 class="title"><i class="fas fa-utensils"></i>Gourmets &amp; Foods<span>グルメ＆フード</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
      <article id="shop" class="shop_list">
        <section>
          <h2 class="title"><i class="fas fa-shopping-bag"></i>Goods &amp; Shops<span>グッズ＆ショップ</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
      <article id="hotel" class="shop_list">
        <section>
          <h2 class="title"><i class="fas fa-hotel"></i>Hotel<span>ホテル</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
      <article id="gym" class="shop_list">
        <section>
          <h2 class="title"><i class="fas fa-dumbbell"></i>Gym<span>ジム</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
      <article id="other" class="shop_list">
        <section>
          <h2 class="title"><i class="far fa-comment-alt"></i>Other<span>その他</span></h2>
          <ul class="flex_pc column3 gap list wrap">
            <li class="yokocho new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="yokocho">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="cafe">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="hotel">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
            <li class="gym">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>ショップ名</strong>
                  <span class="txt">テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。テキスト。</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article>
<?php /*      <article class="news">
        <section>
          <div class="flex_pc column2 title">
            <h2>News<span>新着情報</span></h2>
            <p class="button"><a href="#">一覧を見る</a></p>
          </div>
          <ul class="flex_pc column4 gap list wrap">
            <li class="event new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="food new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="shop new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="shop">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article> */ ?>
<?php include './main_common.php'; ?>
    </main>
<?php /*
コンテンツ領域1280px （SP時に640px、320pxに可変しやすく綺麗に見える）
アートボード1440px Macbook15.4インチに丁度いい
*/ ?>
<?php include './footer.php'; ?>
    <script src="./js/index.js"></script>
  </body>
</html>
