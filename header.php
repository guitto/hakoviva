    <header id="header">
      <section itemscope itemtype="http://#/#">
<?php
  if ($set_id === "index") {
?>
        <h1 class="logo"><a href="#"><img src="./common/img/logo.svg" width="334" height="73" alt="ハコビバ HAKOVIVA"></a></h1>
<?php
  } else {
?>
        <p class="logo"><a href="/"><img src="./common/img/logo.svg" width="334" height="73" alt="ハコビバ HAKOVIVA"></a></p>
<?php
  }
?>
        <nav class="global_navigation">
          <ul class="flex_pc">
            <li><a href="<?php if ($set_id != "index") {echo "/";} ?>#overview"><strong>About HAKOVIVA</strong>ハコビバについて</a></li>
<?php // 20191025保留            <li><a href="/shop.php"><strong>Shop</strong>店舗紹介</a></li> ?>
<?php // 20191025保留            <li><a href="./#/"><strong>News</strong>新着情報</a></li> ?>
            <li><a href="#access"><strong>Access</strong>アクセス・施設案内</a></li>
<?php //            <li><a href="./#/"><strong>Others</strong>その他</a></li>?>
          </ul>
 <?php /*         <div class="iconlink">
            <a href="#" target="_blank" class="fab fa-facebook-f"></a>
            <a href="#" target="_blank" class="fab fa-twitter"></a>
            <a href="#" target="_blank" class="fab fa-instagram"></a>
          </div> */ ?>
 <?php /* 20191025保留         <div>
            <dl class="language">
              <dt>Language</dt>
<?php
  $language = array("en"=>"English", "cn"=>"簡体字", "jp"=>"日本語");
  $i = 0;
  foreach ($language as $key => $value) {
    if ($key === "jp") {
      $url = " href=\"/\"";
    } else {
      $url = " href=\"/$key/\"";
    }
    if ($key === $_SERVER["REQUEST_URI"] || (($key != $_SERVER["REQUEST_URI"]) && $key=="jp" && $i ===0)) {
      $current = " class=\"current\"";
      $url = "";
      $i++;
    } else {
      $current = "";
    }
    echo "\t\t\t  <dd$current><a$url>$value</a></dd>\n";
  }
?>
            </dl>
<?php //AA文字サイズ変更 ?>
          </div> */ ?>
        </nav>

<?php /*
        <ul class="breadcrumb flex">
          <li>
            <a href="http://#/" itemprop="url">
              <span itemprop="title">ホーム</span>
            </a>
          </li>
          <li>
            <a href="http://#/#/#">
              <span itemprop="title">第一階層</span>
            </a>
          </li>
          <li>
            <a href="http://#/#/#">
              <span itemprop="title">第二階層</span>
            </a>
          </li>
          <li>
            <a href="http://#/#/#">
              <span itemprop="title">第三階層</span>
            </a>
          </li>
        </ul>
*/?>
      </section>
    </header><!-- /header -->