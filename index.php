<!DOCTYPE html>
<html lang="ja">
  <head>
<?php
  // 初期設定
  $set_id = "index";
  $title_name = "集まる、広がる。この「場」から。";
  $keywords = "HAKOVIVA,ハコビバ,函館駅前横丁";
  $description = "ＪＲ函館駅前に複合商業施設「ハコビバ」がオープン。函館駅前横丁のほか、スポーツジム、コンビニ、飲食店など20店舗以上からなる複合商業施設です。函館市民をはじめ日本全国、世界各国の人々が集い交流し、みんなに幸せを「運ぶ場」として新しい文化を創造していきます。";
?>
<?php include './head.php'; ?>
    <link href="./css/index.css" rel="stylesheet" type="text/css">
  </head>

  <body id="<?php echo $set_id; ?>">
<?php include './header.php'; ?>
    <main id="main">
      <article class="branding">
        <p class="image"><?php
          include "./images/index_branding.svg";
          // <img src="./images/index_branding.svg" alt="">;
        ?></p>
        <p class="caption">集まる、広がる。<br class="sp">この「場」から。</p>
      </article>
      <article id="overview">
        <p class="opening"><strong>12<span>/</span>7</strong><span>土</span> GRAND OPEN</p>
        <section class="overview">
          <div class="flex_pc fx_row_r">
            <p class="image svg1_1"><img src="./images/index_svg1_1.svg" width="197" height="184" alt=""></p>
            <div class="flex fx_row_r grow2 catch">
              <h2 class="txt_vrl">函館駅前に、<br>あたらしい「顔」。</h2>
              <p class="txt_vrl ">ＪＲ函館駅前に複合商業施設<br>「ハコビバ」がオープン。</p>
              <p class="txt_vrl">函館駅前横丁のほか、スポーツジム、<br>コンビニ、飲食店などの店舗からなる<br>複合商業施設です。</p>
              <p class="txt_vrl">函館市民をはじめ、日本全国、<br>世界各国の人々が集い交流し、<br>みんなに幸せを「運ぶ場」として<br>新しい文化を創造していきます。</p>
            </div>
          </div>
          <p class="image svg1_2"><img src="./images/index_svg1_2.svg" width="87" height="193" alt=""></p>
        </section>
        <section class="sidestreet">
          <div class="flex_pc column2">
            <div>
              <p class="image"><img src="./images/index_image2_1.jpg" srcset="./images/index_image2_1.jpg 1x, ./images/index_image2_1@2x.jpg 2x" alt="函館駅前横丁"></p>

            </div>
            <div class="catch">
              <h2><img src="./common/img/yokocho_logo.svg" width="446" height="108" alt="函館駅前横丁"></h2>
              <p>函館を代表する道産食品やお土産品を販売する物販店や、美味しい食材を提供する飲食店などが昭和の街並みをイメージした空間に軒を連ねます。</p>
<?php //20191025保留              <p class="button"><a href="#">詳しく見る</a></p>?>
            </div>
          </div>
        </section>
        <section class="tenant sp_slider">
          <ul class="flex column4 gap">
            <li style="background-image:url(./images/index_image3_1.jpg);">
              <a href="https://lagent.jp/hakodate-ekimae/" target="_blank" class="no_icon">
                <strong class="image"><img src="./common/img/lagent_logo.svg" width="178" height="55" alt="La&rsquo;gent" class="ml1"></strong>
<?php /*                <strong>ホテル</strong>
                <span>Hotel</span> */ ?>
              </a>
            </li>
            <li style="background-color: #82736b;">
              <span>
                <strong class="image"><img src="./common/img/goldsgym_logo2.svg" width="178" height="178" alt="Gold&rsquo;s GYM"></strong>
<?php /*                <strong>スポーツジム</strong>
                <span>Sports Gym</span> */ ?>
              </span>
            </li>
            <li style="background-image:url(./images/index_image3_3.jpg);">
              <span>
                <strong>ゲートサイド</strong>
                <span>Gate Side</span>
              </span>
            </li>
            <li style="background-image:url(./images/index_image3_4.jpg);">
              <span>
                <strong>ステーションサイド</strong>
                <span>Station Side</span>
              </span>
            </li>
          </ul>
        </section>
<?php /* 20191025保留        <section class="shop">
          <h2 class="title">Shop<span>店舗紹介</span></h2>
          <div class="tab_index wd100">
            <ul class="flex column2">
              <li>お店を探す</li>
              <li><a href="#">フロアマップから探す</a></li>
            </ul>
            <div>
              <div>
                <h3 class="search"><i class="fas fa-search"></i>&nbsp;カテゴリーから探す</h3>
                <ul class="flex column5_pc column2_sp h_gap">
                  <li><a href="/shop.php#food">グルメ＆フード<br class="pc">（レストラン・食堂・菓子・土産）</a></li>
                  <li><a href="/shop.php#shop">グッズ＆ショップ<br class="pc">（物販系）</a></li>
                  <li><a href="https://lagent.jp/hakodate-ekimae/" target="_blank" class="no_icon">ホテル</a></li>
                  <li><a href="/shop.php#gym">スポーツジム</a></li>
                  <li><a href="/shop.php#other">その他<br class="pc">（貸衣装、マッサージなど）</a></li>
                </ul>
              </div>
            </div>
          </div>
        </section> */?>
      </article>
<?php /*      <article class="news">
        <section>
          <div class="flex_pc column2 title">
            <h2>News<span>新着情報</span></h2>
            <p class="button"><a href="#">一覧を見る</a></p>
          </div>
          <ul class="flex_pc column4 gap list wrap">
            <li class="event new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="food new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="shop new">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
            <li class="shop">
              <a href="#">
                <span class="img_area" style="background-image:url(./images/index_image2_1.jpg);"></span>
                <span>
                  <strong>OPENING EVENTを開催しました</strong>
                  <span class="txt">12月○○日に函館駅前にてオープニングイベントが開催されました。</span>
                  <span class="date">0000.00.00 UP →</span>
                </span>
              </a>
            </li>
          </ul>
        </section>
      </article> */ ?>
<?php include './main_common.php'; ?>
    </main>
<?php /*
コンテンツ領域1280px （SP時に640px、320pxに可変しやすく綺麗に見える）
アートボード1440px Macbook15.4インチに丁度いい
*/ ?>
<?php include './footer.php'; ?>
    <script src="./js/index.js"></script>
  </body>
</html>
