
      <article id="access" class="access">
        <section class="no_mw">
          <div class="title flex_pc column2 fx_row">
            <h2>Access<span>アクセス・施設情報</span></h2>
            <p class="image"><img src="./common/img/common_svg1.svg" width="684.54" height="150" alt=""></p>
          </div>
          <div class="map">
            <div class="accordion_js2">
              <a href="javascript: void(0);"></a>
              <div class="adress">
                <p class="img_area"></p>
                <p class="logo"><img src="./common/img/logo_w.svg" alt="HAKOVIVA"></p>
                <p>〒040-0063<br>北海道函館市若松町12番8号<?php //<br>0000-00-000?></p>
                <p class="button"><a href="https://goo.gl/maps/qWJX4d1eJagnSnUx5" target="_blank">詳しくはこちら</a></p>
              </div>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2975.5807631271337!2d140.72504261573542!3d41.774235279886085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f9ef398d089b62f%3A0x7b6d3662cf308a45!2z44CSMDQwLTAwNjMg5YyX5rW36YGT5Ye96aSo5biC6Iul5p2-55S677yR77yS4oiS77yY!5e0!3m2!1sja!2sjp!4v1569921376640!5m2!1sja!2sjp" width="100%" height="100%" allowfullscreen=""></iframe>
        </section>
      </article>
<?php /*      <article>
        <section class="link_list">
          <ul class="flex_pc gap column5">
            <li>
              <h3>〇〇〇〇〇〇〇〇〇〇</h3>
              <ul class="link">
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
              </ul>
            </li>
            <li>
              <h3>〇〇〇〇〇〇〇〇〇〇</h3>
              <ul class="link">
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
              </ul>
            </li>
            <li>
              <h3>〇〇〇〇〇〇〇〇〇〇</h3>
              <ul class="link">
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
              </ul>
            </li>
            <li>
              <h3>〇〇〇〇〇〇〇〇〇〇</h3>
              <ul class="link">
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
              </ul>
            </li>
            <li>
              <h3>〇〇〇〇〇〇〇〇〇〇</h3>
              <ul class="link">
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
                <li><a href="#">〇〇〇〇〇〇〇〇</a></li>
              </ul>
            </li>
          </ul>
        </section>
      </article> */ ?>