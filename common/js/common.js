//ウインドウサイズ（ipadのモニタサイズ）
var $min_pc = 768;
//ウインドウサイズ（横幅のMAXのモニタサイズ）
var $max_pc = 1200;
//スマートフォンチェック
var sp_check = 0;
var ua = navigator.userAgent;
if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
  sp_check++;
}
var w = $(window).width();
var h = $(window).height();

//間隔のデフォルト値
var $mg;
if (w >= $min_pc) {
  $mg = 100; //PC用
} else {
  $mg = 32; //SP用
}


// ===== 以下固定 =====
$(document).ready(function(){

  /* IE9FLEX対応*/
  var userAgent = window.navigator.userAgent.toLowerCase();
  if( userAgent.match(/(msie|MSIE)/)) {
    var isIE = true;
    var ieVersion = userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/)[3];
    ieVersion = parseInt(ieVersion);
    if(ieVersion<10){
      $(function(){
        flexibility(document.documentElement);
      });
    }
  }

  /* 表示時のFadeIn ※原則使用しない
  // $('body').css('opacity','1');
  //$('input[type="checkbox"]').prop('checked', false);

  /* タブレット用ビューポート ※原則使用しない
  $(function(){
    if (sp_check > 0) {
      $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1320">');
    }
  }); */
  //var offset = 300;

});

//スクロールで起動
$(window).scroll(function(){
  fadein();// 画像fadein
  var w = $(window).width();
//  if (w >= $min_pc) {
    var h = 40;
    var scrollValue = $(window).scrollTop();
    if(scrollValue > h){
      $('.gotop').removeClass('visible');
      $('header').addClass('-scrolled');
    } else {
      $('.gotop').addClass('visible');
      $('header').removeClass('-scrolled');
    }
//  }
});

//.画像のfadeIn,fadeOut（classは_parts）
function fadein() {
  var win_h = $(window).innerHeight();
  var scroll = $(window).scrollTop();
  $("img").each(function(index, element) {
    var img_h = ($(this).innerHeight())/2;
    var img_p=$(this).offset().top;
    if ((img_p-scroll) < (win_h-img_h) && (img_p-scroll+img_h) > 0) {
      $(this).addClass("show_image");
    } else {
      $(this).removeClass("show_image");
    }
  });
}


/* modal_win */
//.画像のMAXサイズと送り番号の設定
function modalin($href, $number) {
  var img = new Image();
  img.src = $href;
  setTimeout(function(){
    var w = $(window).width();
    var h = $(window).height();
    var width  = img.width;
    var height = img.height;
    if (width > 0 || height > 0) {
      var $i = 0;
      while ($i > 0) {
        width  = img.width;
        height = img.height;
        if (width> 0 && height>0) {
          $i++;
        }
      }
    }
    var $prev = $number - 1;
    var $next = $number + 1;
    var styleset;
    if (width >0 && height >0 ) {
      var wd;
      var hg;
      if (width > height) {
        if (width>w) {
          wd = "max-width: " + (w - $mg) + "px";
          hg = "height: auto";
        } else if (height>h) {
          wd = "width: auto";
          hg = "max-height: " + (h - $mg) + "px";
        } else {
          wd = "width: " + width + "px";
          hg = "height: " + height + "px";
        }
      } else {
        if (height>h) {
          wd = "width: auto";
          hg = "max-height: " + (h - $mg) + "px";
        } else if (width>w) {
          wd = "max-width: " + (w - $mg) + "px";
          hg = "height: auto";
        } else {
          wd = "width: " + width + "px";
          hg = "height: " + height + "px";
        }
      }
      styleset = "style=\""+ wd + "; " + hg + ";\"";
    }
    if ($prev < 0) {
      $prev = ($modal_img.length - 1);
    }
    if ($next >= $modal_img.length) {
      $next = 0;
    }
    $(".modal_win").html( '<p><img src="'+$href+'"'+styleset+'><a class="close"></a></p>' );
    if ($modal_img_caption[$number]) {
      $(".modal_win").children("p").append("<span class=\"caption\">" + $modal_img_caption[$number] + "</span>");
    }
    if ($modal_img_caption.length>0) {
      $(".modal_win").children("p").append("<a rel=\"" + $prev + "\" class=\"change prev\"></a>");
      $(".modal_win").children("p").append("<a rel=\"" + $next + "\" class=\"change next\"></a>");
    }
  },300);
}



$(function(){
  $('<div class="modal_win"></div>').prependTo('body');
  $("area.modal,a.modal").click(function() {
    var $href = $(this).attr('href');
    var $rel = $(this).attr('rel');
    var w = $(window).width();
    var h = $(window).height();
    var $number = 0;
    var $i=0;
    $modal_img.forEach(function( value ) {
      if (($href == value) && ($rel == $modal_img_caption[$i])) {
        $number = $i;
      }
      $i++;
    });
    $(".modal_win").fadeIn("fast");
    $(".modal_win").css("display","flex");
    if ($href.indexOf(".jpg") > 0 || $href.indexOf(".jepg") > 0 || $href.indexOf(".gif") > 0 || $href.indexOf(".png") > 0 || $href.indexOf(".svg") > 0) {
    //画像の場合
      modalin($href,$number);
    } else if ($href.indexOf("#") >= 0 )  {
    //ページ内のデータを読み込み
      var $html = $($href).html();
      //videoがあった場合 自動スタートを代入
      if ($html.indexOf("<video") > -1) {
        $html = $html.split("\<video").join("\<video controls autoplay");
      }
      $(".modal_win").html( "<div><div>" + $html + "</div><a class=\"close\"></a></div>" );
      var ineer_hg = $(".modal_win > div > div").height();
      if (w < $min_pc && sp_check > 0 && ineer_hg >= (h - $mg)) {
        $(".modal_win > div > div").css("padding-bottom", ($mg * 4));
      }
    } else if ($href) {
    //iframeの場合
      $(".modal_win").html( "<div style=\"width: 75%;\"><iframe width=\"100%\" height=\"" + (h*0.75) + "\" src=\""+$href+"\"></iframe><a class=\"close\"></a></div>" );
    }
    return false;
  });
  //  $modal_img = [];
  //  $modal_img_caption = [];
  $(".modal_win").click(function() {
    //呼び出し画像の変更
    if ($(event.target).closest('a.change').length) {
      var $rel = $(event.target).closest('a.change').attr('rel') - 0;
      modalin($modal_img[$rel], $rel);
    } else if(!$(event.target).closest('p').length || !$(event.target).closest('div').length || $(event.target).closest('.close').length) {
      $(".modal_win").fadeOut("fast");
      $(".modal_win").html(" ");
    }
  });

  //PCでは電話番号が発火しない
  var ua = navigator.userAgent.toLowerCase();
  var isMobile = /iphone/.test(ua)||/android(.+)?mobile/.test(ua);

  if (!isMobile) {
      $("body").on("click","a[href^=\"tel:\"]", function() {
          e.preventDefault();
      });
  }

  //[.tab_js1] タブの開閉
  /*
  一つだけ開くタイプのタブ（クリックしたものだけが表示される）
  タブは上が自動的に指定される。下にしたければCSSでひっくり返す。
  選択中のタブ→ .open
  表示中のblock→ .current
  */
  $("[class*=\"tab_js1\"] > :nth-child(1) > :not(ul):not(.tab_menu)").click(function() {
    if($(this).parents().hasClass("tab_js1") || ($(this).parents().hasClass("tab_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("tab_js1_sp") && w < $min_pc)) {
      tab_js1(this);
    }
  });
  $("[class*=\"tab_js1\"] > :nth-child(1)  .tab_menu> *").click(function() {
    if($(this).parents().hasClass("tab_js1") || ($(this).parents().hasClass("tab_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("tab_js1_sp") && w < $min_pc)) {
      tab_js1(this);
    }
  });
  $("[class*=\"tab_js1\"] > :nth-child(1) >ul> *").click(function() {
    if($(this).parents().hasClass("tab_js1") || ($(this).parents().hasClass("tab_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("tab_js1_sp") && w < $min_pc)) {
      tab_js1(this);
    }
  });

  function tab_js1(tab_js1) {
    var $x = 0;
    var $i = $(tab_js1).index();
    $.cookie("tab_js1_" + $(tab_js1).parents("[class*=\"tab_js1\"]").attr("tabindex"), $i, { expires: 7 });
    $(tab_js1).parent().children().removeClass("open");
    $(tab_js1).addClass("open");
    $(tab_js1).parents("[class*=\"tab_js1\"]").children(":nth-child(2)").children().each(function(index, element) {
      $(this).hide().removeClass("current");
      if( $x == $i ) {
        $(this).fadeIn().addClass("current");
      }
      $x++;
    });
  }


  //[.sort_js1] タブの開閉
  /*
  一つだけ開くタイプのタブ（クリックしたものだけが表示される）
  タブは上が自動的に指定される。下にしたければCSSでひっくり返す。
  選択中のタブ→ .open
  表示中のblock→ .current
  */
  $("[class*=\"sort_js1\"] > :nth-child(1) > :not(ul):not(.tab_menu)").click(function() {
    if($(this).parents().hasClass("sort_js1") || ($(this).parents().hasClass("sort_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("sort_js1_sp") && w < $min_pc)) {
      sort_js1(this);
    }
  });
  $("[class*=\"sort_js1\"] > :nth-child(1)  .tab_menu> *").click(function() {
    if($(this).parents().hasClass("sort_js1") || ($(this).parents().hasClass("sort_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("sort_js1_sp") && w < $min_pc)) {
      sort_js1(this);
    }
  });
  $("[class*=\"sort_js1\"] > :nth-child(1) >ul> *").click(function() {
    if($(this).parents().hasClass("sort_js1") || ($(this).parents().hasClass("sort_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("sort_js1_sp") && w < $min_pc)) {
      sort_js1(this);
    }
  });

  function sort_js1(sort_js1) {
    var $x = 0;
    var $i = $(sort_js1).attr("class");
    if ($i === void 0) {
      $i = "";
    }
    $i = $i.split(" ");
    if ($i.indexOf("open") < 0) {
      $.cookie("sort_js1_" + $(sort_js1).parents("[class*=\"sort_js1\"]").attr("tabindex"), $i[0], { expires: 7 });
      $(sort_js1).parent().children().removeClass("open");
      $(sort_js1).addClass("open");
      $(sort_js1).parents("[class*=\"sort_js1\"]").children(":nth-child(2)").children().each(function(index, element) {
        $(this).hide().removeClass("current");
  //      $i.forEach(function( value ) {
          if($(this).attr("class").indexOf($i[0]) > -1) {
            $(this).fadeIn().addClass("current");
          }
  //        break;
  //      });
        $x++;
      });
    }
  }


  //[.accordion_js1] アコーディオンの開閉
  /*
  一つだけ開くタイプのアコーディオン（クリックしたものだけが表示される）
  縦/横は classに flex を付けて制御（floatは禁止）
  奇数がボタン、偶数がが表示画面となるので順番に注意
  選択中のボタン→ .open
  表示中のblock→ .open + *
  */
  $("[class*=\"accordion_js1\"] > *").click(function() {
    var w = $(window).width();
    if($(this).parents().hasClass("accordion_js1") || ($(this).parents().hasClass("accordion_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("accordion_js1_sp") && w < $min_pc)) {
      var $j = $(this).parent().attr("tabindex");
      var $i = $(this).index();
      var $class_check = $(this).parent().attr("class");
      $(this).parent().children(":nth-child(2n-1)").removeClass("onen");
      $(this).parent().children(":nth-child(2n)").fadeOut();
  /*    if (($class_check.indexOf("flex") > -1) && ($class_check.indexOf("fx_clm") < 0)) {
        $(this).parent().children(":nth-child(2n)").css("width", 0);
      } else {
        $(this).parent().children(":nth-child(2n)").css("height", 0);
      }*/
      $(this).addClass("open");
  /*    if (($class_check.indexOf("flex") > -1) && ($class_check.indexOf("fx_clm") < 0)) {
        $(this).next().css("width", $accordion_js1w[$j][$i]);
      } else {
        $(this).next().css("height", $accordion_js1h[$j][$i]);
      }*/
      $(this).next().fadeIn();
      $.cookie("accordion_js1_" + $j, $i, { expires: 7 });
    }
  });

  //[.accordion_js2] アコーディオンの開閉
  /*
  複数開くタイプのアコーディオン（クリックしたものが開閉する）
  縦/横は classに flex を付けて制御（floatは禁止）
  奇数がボタン、偶数がが表示画面となるので順番に注意
  選択中のボタン→ .open
  表示中のblock→ .open + *
  */
  $("[class*=\"accordion_js2\"] > *").click(function() {
    var w = $(window).width();
    if($(this).parents().hasClass("accordion_js2") || ($(this).parents().hasClass("accordion_js2_pc") && w >= $min_pc) || ($(this).parents().hasClass("accordion_js2_sp") && w < $min_pc)) {
      var $j = $(this).parent().attr("tabindex");
      var $i = $(this).index();
      var $x = $(this).attr("class");
      if ($x === void 0) {
        $x = "";
      }
      var $class_check = $(this).parent().attr("class");
      if (($x.indexOf("open") < 0)) {
        $(this).addClass("open");
  /*      if (($class_check.indexOf("flex") > -1) && ($class_check.indexOf("fx_clm") < 0)) {
          $(this).next().css("width", $accordion_js2w[$j][$i]);
        } else {
          $(this).next().css("height", $accordion_js2h[$j][$i]);
        }*/
        $(this).next().fadeIn();
      } else {
        $(this).removeClass("open");
  /*      if (($class_check.indexOf("flex") > -1) && ($class_check.indexOf("fx_clm") < 0)) {
          $(this).next().css("width", 0);
        } else {
          $(this).next().css("height", 0);
        }*/
        $(this).next().fadeOut();
      }
      var $ii = "";
      $(this).parent().children().each(function(index, element) {
        if ($(this).hasClass("open")) {
          if ($ii === "") {
            $ii = $(this).index();
          } else {
            $ii += " " + $(this).index();
          }
        }
      });
      $.cookie("accordion_js2_" + $j, $ii, { expires: 7 });
    }
  });

 /* // gotopボタン
  $(".gotop").click(function(){
    var speed = 500;

    $("html, body").animate({scrollTop: 0}, speed, "swing");
    return false;
  });*/

  // アンカーリンク（同一ページ）
  $("body").on("click","area[href^=\"#\"], a[href^=\"#\"]", function() {
    ankerlink(this,500);
  });
});
function ankerlink($this,speed) {
  var w = $(window).width();
  var head_h;
  if (w < $min_pc) {
    head_h = $("header .logo").outerHeight();
  } else {
    head_h = $("header").outerHeight();
  }
  var href= $($this).attr("href");
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - head_h;
  //.modalの例外処理
  if (!$($this).hasClass("modal")) {
    $("html, body").animate({scrollTop:position}, speed, "swing");
  }
  if ($(".btnHamburger").hasClass("open")) {
    $(".btnHamburger").removeClass("open");
    $("header .btnHamburger+nav").hide();//.fadeOut();
  }
  return false;
}

// [table1b] スクロール時の左メニューの固定
$(".table1b").scroll(function() {
    var $scroll = $(this).scrollLeft();
    $(".table1b tr>:first-child").css("left", $scroll);
});

// Hamburgerの起動
$("header").on("click",".btnHamburger", function() {
   var a = $("header .btnHamburger+ul").index(this);
  if ($(this).hasClass("open")) {
     $(this).removeClass("open");
     $("header .btnHamburger+nav").hide();//.fadeOut();
  } else {
     $(this).toggleClass("open");
     $("header .btnHamburger+nav").eq(a).fadeIn().css("display","flex");
  }
});
$(".global_navigation").on("click",".button", function() {
  if ($(this).hasClass("-open")) {
    $(this).removeClass("-open");
  } else {
    $(this).addClass("-open");
    var $i = $(this).append().position().top;
    $(".global_navigation .nav").animate({scrollTop: $i}, 500, "swing");
  }
});



setTimeout( function () {
  var w = $(window).width();
  var h = $(window).height();
  var $css = "";
  //外部リンクにtarget="_blank"挿入
  $("main a").not(".modal").each(function(index, element) {
    var $href = $(this).attr("href");
    var $target = $(this).attr("target");
    if (($target === void 0) && $href.indexOf("http") === 0 ) {
      $(this).attr("target", "_blank");
    }
  });
  //flex column のカラム数をカウントし wrapを自動挿入する
  $("[class*=\"flex\"][class*=\"column\"]").each(function(index, element) {
    if (!$(this).hasClass("wrap")) {
      var $i = $(this).children().length;
      var $class = $(this).attr('class').split(" ");
      var x;
      for (var $j = 0; $j < $class.length; $j++) {
        if ($class[$j].indexOf("column") > -1) {
          $x = $class[$j].replace( /column/g , "" ) ;
          if (($class[$j].indexOf("pc") > -1) || ($class[$j].indexOf("sp") > -1)) {
            var w = $(window).width();
            var $y = $class[$j].split("_");
            $y[0] = $y[0].replace( /column/g , "" ) ;
            if ((w >= $min_pc) && ($y[1] == "pc")) {
              $x = $y[0] - 0;
              break;
            } else if ((w < $min_pc) && ($y[1] == "sp")) {
              $x = $y[0] - 0;
              break;
            }
          } else {
            $x = $x - 0;
            break;
          }
        }
      }
      if ($i > $x) {
        $(this).addClass("wrap");
      }
    }
  });
  //画像fadein
  fadein();
  //画像にwidthとheightを自動挿入
  $("img").each(function(index, element) {
    var $img = new Image();
    $img.src = $(this).attr('src');
    var $width  = $img.width;
    var $height = $img.height;
    var $image_w = $(this).parent().width();
    if($image_w < $width) {
      $height = $height * $image_w / $width;
      $width  = $image_w;
    }
    $(this).attr("width", $width);
    $(this).attr("height", $height);
  });

  //modalのpreloadおよび画像有無の確認
  $modal_img = [];
  $modal_img_caption = [];
  var $i = 0;
  $(".modal").each(function(index, element) {
    var $href = $(this).attr('href');
    if ($href.indexOf(".jpg") > 0 || $href.indexOf(".jepg") > 0 || $href.indexOf(".gif") > 0 || $href.indexOf(".png") > 0 || $href.indexOf(".svg") > 0) {
      $("<img>").attr("src", $href);
      //console.log($href);
      $modal_img[$i] = $href;
      $modal_img_caption[$i] = $(this).attr('rel');
      $i++;
    }
  });

  //[.text] <br>の後ろに全角スペース挿入
  $(".text>br").after("<i class=\"space\"></i>");

  //[.link] リストとの混在
  $(".link a").each(function(index, element) {
      $(this).parent().addClass("no_list");
  });


  //[.tab_js1] タブ初期設定[];
  var $jj = 0;
  var $tab1_open_check = [];
  var $tab1_scroll_check = [];
  $("[class*=\"tab_js1\"]").each(function(index, element) {
    if (!($(this).attr("tabindex"))) {
      $(this).attr("tabindex", $jj);
    }
    $tab1_open_check[$jj] = $(this).html().indexOf("open") + $(this).html().indexOf("current");
    $tab1_scroll_check[$jj] = $(this).children().eq(0).html();
    $jj++;
  });
  var $jj = 0;
  var $jjj;
  $("[class*=\"tab_js1\"] > :nth-child(2) > *").each(function(index, element) {
    if($(this).parents().hasClass("tab_js1") || ($(this).parents().hasClass("tab_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("tab_js1_sp") && w < $min_pc)) {

      var $i = $.cookie("tab_js1_" + $(this).parent().parent().attr("tabindex"));
      var $j = $(this).parent().parent().attr("tabindex");
      if ($jjj != $j) {
        $jjj = $j;
        $jj = 0;
      }
      if ($i === void 0) {
        $i = "";
      }
      if ($tab1_open_check[$j] < 0 || $i)　{
        var $x = $(this).attr("class");
        if ($x === void 0) {
          $x = "";
        }
        if ($x.indexOf("current") > -1) {
          if ($tab1_scroll_check[$j].indexOf("ul") > -1) {
            $(this).parent().prev().children().children().eq($jj).removeClass("open");
          }
          if ($tab1_scroll_check[$j].indexOf("ul") > -1) {
            $(this).parent().prev().find(".tab_menu").children().eq($jj).removeClass("open");
          } else {
            $(this).parent().prev().children().eq($jj).removeClass("open");
          }
        }
        if (($(this).index() != $i) || (($(this).index() > 0) && !$i)){
          $(this).hide();
          $(this).removeClass("current");
        } else {
          $(this).addClass("current");
          if ($tab1_scroll_check[$j].indexOf("ul") > -1) {
            $(this).parent().prev().children().children().eq($i).addClass("open");
          }
          if ($tab1_scroll_check[$j].indexOf("ul") > -1) {
            $(this).parent().prev().find(".tab_menu").children().eq($i).addClass("open");
          } else {
            $(this).parent().prev().children().eq($i).addClass("open");
          }
        }
      } else {
        if (!$(this).hasClass("current")) {
          $(this).hide();
        }
      }
      $jj++;
    }
  });


  //[.sort_js1] タブ初期設定[];
  var $jj = 0;
  var $sort1_open_check = [];
  var $sort1_scroll_check = [];
  $("[class*=\"sort_js1\"]").each(function(index, element) {
    if (!($(this).attr("tabindex"))) {
      $(this).attr("tabindex", $jj);
    }
    var $i = $.cookie("sort_js1_" + $(this).attr("tabindex"));
    if ($i === void 0 || $i === "") {
      $(this).children(":first-child").children(":first-child").addClass("open");
    } else {
      $(this).children(":first-child").find("." + $i).addClass("open");
    }
    $sort1_open_check[$jj] = $(this).html().indexOf("open") + $(this).html().indexOf("current");
    $sort1_scroll_check[$jj] = $(this).children().eq(0).html();
    $jj++;
  });
  var $jj = 0;
  var $jjj;
  $("[class*=\"sort_js1\"] > :nth-child(2) > *").each(function(index, element) {
    if($(this).parents().hasClass("sort_js1") || ($(this).parents().hasClass("sort_js1_pc") && w >= $min_pc) || ($(this).parents().hasClass("sort_js1_sp") && w < $min_pc)) {
      var $i = $.cookie("sort_js1_" + $(this).parent().parent().attr("tabindex"));
      var $j = $(this).parent().parent().attr("tabindex");
      if ($jjj != $j) {
        $jjj = $j;
        $jj = "";
      }
      if ($i === void 0) {
        $i = "";
      }
      if ($sort1_open_check[$j] < 0 || $i)　{
        var $x = $(this).attr("class");
        if ($x === void 0) {
          $x = "";
        }
        if ($x.indexOf($i) > -1) {
          $(this).fadeIn().addClass("current");
        } else {
          $(this).hide().removeClass("current");
        }
      }
      $jj++;
    }
  });

  //[.accordion_js1] アコーディオン初期設定
/*  $accordion_js1w = [];
  $accordion_js1h = [];*/
  var $j = 0;
  $("[class*=\"accordion_js1\"]").each(function(index, element) {
    if($(this).hasClass("accordion_js1") || ($(this).hasClass("accordion_js1_pc") && w >= $min_pc) || ($(this).hasClass("accordion_js1_sp") && w < $min_pc)) {
      var $ad1_js;
      if (!($(this).attr("tabindex"))) {
  /*      $accordion_js1w[$j] = [];
        $accordion_js1h[$j] = [];*/
        $(this).attr("tabindex", $j);
        $ad1_js = $.cookie("accordion_js1_" + $j);
      } else {
        var $jj = $(this).attr("tabindex");
  /*      $accordion_js1w[$jj] = [];
        $accordion_js1h[$jj] = [];*/
        $ad1_js = $.cookie("accordion_js1_" + $jj);
      }
      if ($ad1_js === void 0) {
        if (($(this).html().indexOf("open") < 0) && ($(this).children().length > 2)) {
          $(this).children().eq(0).addClass("open");
        }
      } else {
        $(this).children().removeClass("open");
        $(this).children().eq($ad1_js).addClass("open");
      }
      var $i = 0;
      $(this).children().each(function(index, element) {
        var $jj;
        if (!($(this).parent().attr("tabindex"))) {
          $jj = $j;
        } else {
          $jj = $(this).parent().attr("tabindex");
        }
  /*      $accordion_js1w[$jj][$i] = $(this).width();
        $accordion_js1h[$jj][$i] = $(this).height();*/
        var $x = $(this).prev().attr("class");
        if ($x === void 0) {
          $x = "";
        }
        if (($x != "open") &&  ($i % 2 != 0)) {
          $(this).hide();
        }
        $i++;
      });
      $j++;
    }
  });


  //[.accordion_js2] アコーディオン初期設定
/*  $accordion_js2w = [];
  $accordion_js2h = [];*/
  var $j = 0;
  $("[class*=\"accordion_js2\"]").each(function(index, element) {
    if($(this).hasClass("accordion_js2") || ($(this).hasClass("accordion_js2_pc") && w >= $min_pc) || ($(this).hasClass("accordion_js2_sp") && w < $min_pc)) {
      var $ad1_js;
      if (!($(this).attr("tabindex"))) {
  /*      $accordion_js2w[$j] = [];
        $accordion_js2h[$j] = [];*/
        $(this).attr("tabindex", $j);
        $ad1_js = $.cookie("accordion_js2_" + $j);
      } else {
        var $jj = $(this).attr("tabindex");
  /*      $accordion_js2w[$jj] = [];
        $accordion_js2h[$jj] = [];*/
        $ad1_js = $.cookie("accordion_js2_" + $jj);
      }
      if ($ad1_js === void 0) {
   //     if (($(this).html().indexOf("open") < 0) && ($(this).children().length < 2)) {
        if ($(this).html().indexOf("open") < 0) {
          $(this).children().eq(0).addClass("open");
        }
      } else {
        $(this).children().removeClass("open");
        var $ad1_js2 = $ad1_js.split(" ");
        for(var $i = 0; $i <= $ad1_js2.length; $i++) {
          $(this).children().eq($ad1_js2[$i]).addClass("open");
        }
      }
      var $i = 0;
      $(this).children().each(function(index, element) {
        var $jj;
        if (!($(this).parent().attr("tabindex"))) {
          $jj = $j;
        } else {
          $jj = $(this).parent().attr("tabindex");
        }
  /*      $accordion_js2w[$jj][$i] = $(this).width();
        $accordion_js2h[$jj][$i] = $(this).height();*/
        var $x = $(this).prev().attr("class");
        if ($x === void 0) {
          $x = "";
        }
        if (($x != "open") &&  ($i % 2 != 0)) {
          $(this).hide();
        }
        $i++;
      });
      $j++;
    }
  });

  // スマートフォン専用
  if (w < $min_pc) {
    //ハンバーガーメニュの挿入
    $("header .global_navigation").before("<p class=\"btnHamburger\"><span class=\"line line_01\"></span><span class=\"line line_02\"></span><span class=\"line line_03\"></span></p>");

    //グローバルナビゲーションをSP用に変更する
    $(".global_navigation .nav li >a").each(function(index, element) {
      if ($(this).next("ul").html() != void 0) {
        var $i = "<span class=\"button\">" + $(this).html() + "</span>\n";
        $(this).before($i);
      }
    });

    //[.table1b]左端のth,td固定
    var $th_w = 0;
    var $i = 0;
    $(".table1b tr>:first-child").each(function(index, element) {
      var $th_w_sub = $(this).innerWidth();
      if ($th_w < $th_w_sub) {
        $th_w = $th_w_sub;
      }
    });
    if ($th_w > (w / 3)) {
      $th_w = (w / 3);
    }
    $css += ".table1b tr {padding-left:" + $th_w + "px;}.table1b tr>:first-child {width:" + $th_w + "px;}";
    var $td_w = [];
    var $jj=0;
    $(".table1b thead tr:last-child>*").each(function(index, element) {
      $jj++;
    });
    var $j=0;
    $(".table1b tr>*").each(function(index, element) {
      if ($j >= $jj) {
        $j=0;
      }
      $(this).addClass("tb1b_td_" + $j);
      var $td_w_sub = $(this).innerWidth();
      if(($td_w_sub > $td_w[$j]) || !$td_w[$j]) {
        $td_w[$j] = $td_w_sub;
      }
      $j++;
    });
    for (var $i=0; $i<= $td_w.length; $i++) {
      $css += ".table1b .tb1b_td_" + $i + " {width:" + $td_w[$i] + "px;}";
    }
    /*$(".table1b tr>:first-child").each(function(index, element) {
      $(this).css("width", $th_w);
      $(this).parent().css("padding-left", $th_w);
    });*/

    //[.table2b]見出しを代入
    var table2_thead = [];
    var $i=0;
    $(".table2b thead>tr>*").each(function(index, element) {
      table2_thead[$i] = $(this).html();
      $i++;
    });
    var $i=0;
    $(".table2b tbody>tr>*").each(function(index, element) {
      var $td = $(this).html();
      $(this).prepend("");
      $(this).html("<div class=\"th\">" + table2_thead[$i] + "</div><div class=\"td\">"+ $td +"</div>");
      $i++;
      if ($i >= table2_thead.length) {
        $i=0;
      }
    });

    // （共通）JS設定cssの書き出し
    $("head").append(
      $('<style/>', {
        html: $css
      })
    );

  }
  // URLのアンカー（#以降の部分）を取得
  var $urlHash = location.hash;
  if($urlHash){
    var head_h;
    if (w < $min_pc) {
      head_h = $("header .logo").outerHeight();
    } else {
      head_h = $("header").outerHeight();
    }
    var position = $($urlHash).offset().top - head_h;
    // $("html, body").scrollTop(position);
    $("html, body").animate({scrollTop: position}, 500, "swing");
  }
  //gotopボタン
  $("body").append("<a href=\"#\" class=\"gotop visible\"></a>");
} , 300 );
